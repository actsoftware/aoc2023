use std::io;
use std::fs::read_to_string;

// Directly stolen from doc.rust-lang.org
// Just reads all the lines, shoves them into a vector.
fn readlines(filename: &str) -> Vec<String> {
    let mut result = Vec::new();
    for line in read_to_string(filename).unwrap().lines(){
        result.push(line.to_string());
    }
    result
}


// Day01 of AOC 2023.
// Done on December 5th, 2023 by act17 - https://fearandloathing.neocities.org
// I started this at like, 5PM on December 5th. Yeah, I'm late to the party.
fn main(){
    let mut input = Vec::new();
    input = readlines("./input.txt");
    println!("{:?}",input);


    // Actual algorithm.
    let mut result: usize = 0;
    for currentstring in input {
        let mut chars: Vec<char> = currentstring.chars().collect();
        let mut charbuffer: Vec<usize> = vec![];

        // Iterate through each character of the current string.
        for currentchar in chars {
            // In the case that its ASCII code corresponds to the range for numbers,
            // push it into the charbuffer.
            if currentchar as usize > 47 && (currentchar as usize) < 58 {
                charbuffer.push((currentchar as usize) - 48);
            }
        }
        
        // Then, wrap 'er up by the first entry in charbuffer times ten,
        // and the last entry times one.
        result = result + (charbuffer.first().unwrap() * 10);
        result = result + charbuffer.last().unwrap();
    }

    println!("{}",result);
}
